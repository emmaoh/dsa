from colorsys import TWO_THIRD


array = [10, 3, 7, 5]
#print array
print(array)

# random indexing 
# Index starts with 0  --> O(1) time complexity. Print the second item 
print(array[1])

#print all the item 
print(array[:]) 

#print first 2 items. [start:stop]. start is inclusive, stop is exclusive. 
print(array[:2])
print(array[1:3])

#print all except the last item
print(array[:-1])

#print all except the last two items 
print(array[:-2])

#In Python, we can mix types in the array. In Java, C, C++ not possible.
array2 = [10.0, 3, 7, 'Adam']

#update 
array2[3] = 'Kevin' 
print(array2)

#This is called linear search O(N)
array3 = [10, 42, 55, 2, 1, 0]
max = array3[0]
for num in array3:
    if num > max:
        max = num
print(max)

min = array3[0]
for num in array3:
    if num < min:
        min = num
print(min)