def dutch_flag(lst):
    i, j = 0, 0
    k = len(lst)-1
    while j <= k:
        if lst[j] < 1:
            lst[i], lst[j] = lst[j], lst[i]
            i += 1
            j += 1
        elif lst[j] > 1:
            lst[j], lst[k] = lst[k], lst[j]
            k -= 1
        else:
            j += 1 
    return lst 
    
lst = [0,1,2,0,0,1,2,1,0]
print(dutch_flag(lst))

#time complexity: O(n)
