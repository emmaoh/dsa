#Interview Question #4
#Construct an algorithm to check whether two words (or phrases) are anagrams or not!
#"An anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once"
#For example: restful and fluster

import collections
def anagram(s1, s2):
    tracker = collections.defaultdict(int)
    for x in s1: tracker[x] += 1
    for x in s2: tracker[x] -= 1
    return all(x == 0 for x in tracker.values())


    # if len(s1) != len(s2):
    #     return False 
    # # sort the letters of the strings then compare the letter with the same index.
    # # bottleneck: it has O(nlogN)
    # s1 = sorted(s1)
    # s2 = sorted(s2) 
    # # O(N)
    # for i in range(len(s1)):
    #     if s1[i] != s2[i]:
    #         return False 
    #     return True 
    # #overall running time is O(nlogn) + O(N) = O(nlogn)
    

#     charMap = {}
#     for char in s1:
#         if char not in charMap:
#             charMap[char] = 0
#         charMap[char] += 1
#     for char in s2:
#         if char in charMap: 
#             charMap[char] -= 1
#             if charMap[char] == 0:
#                 del charMap[char]
#     if len(charMap) == 0:
#         return True 
#     else:
#         return False 
# Time: O(n); Space: O(1)

s1 = 'restful'
s2 = 'fluster'
print(anagram(s1, s2))
    