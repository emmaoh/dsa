#Integer reversion problem overview
#Interview Question #3
#Our task is to design an efficient algorithm to reverse a given integer. For example if the input of the algorithm is 1234 then the output should be 4321.

def reverse_int(number):
    reversed_num = 0 
    while number > 0:
        remainder = number % 10 
        number = number // 10
        reversed_num = reversed_num * 10 + remainder
    return reversed_num







    # lst_num = list(str(number))
    # l,r = 0 , len(lst_num)-1
    # while l < r:
    #     lst_num[l], lst_num[r] = lst_num[r], lst_num[l]
    #     l += 1
    #     r -= 1
    # return int(''.join(lst_num))


number = 345
print(reverse_int(number))