# Interview Question #2
#"A palindrome is a string that reads the same forward and backward"
#For example: radar or madam
#Our task is to design an optimal algorithm for checking whether a given string is palindrome or not! 

def palindrome(s):
    if s == reverse(s):
        return True
    return False 

def reverse(s):
    data = list(s)
    l, r = 0, len(s)-1
    while l < r:
        data[l], data[r] = data[r], data[l]
        l += 1
        r -= 1
    return ''.join(data)
    #transfer the list of letter to string 
           
    #Time complexity: O(N) Linear running time where N is the number of letters in string N = len(s)


    # if s == s[::-1]:
    #     return True
    # return False 

s = 'Madam'
print(palindrome(s))