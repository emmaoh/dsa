# FIFO (First In First Out)

class Queue: #abstract data type

    def __init__(self):
        self.queue = [] #underlying data structure 

    #O(1) running time 
    def is_empty(self):
        return self.queue == []
    
    #O(1) running time 
    def enqueue(self, data):
        self.queue.append(data)

    #O(N) linear running time . Once you remove the first item, you have to shift each item to the left. 
    # How to solve this problem? Use Doubly linked list O(1) for both enqueue and dequeue. 
    def dequeue(self):
        if self.size_queue() != 0:
            data = self.queue[0]
            del self.queue[0]
            return data
        else:
            return -1  

    #O(1) running time 
    def peek(self):
        return self.queue[0]

    #O(1) running time 
    def size_queue(self):
        return len(self.queue)

    
queue = Queue()
queue.enqueue(1)
queue.enqueue(2)
queue.enqueue(3)
print("Size: %d" % queue.size_queue())
print("Dequeue: %d" % queue.dequeue())
print("Dequeue: %d" % queue.dequeue())
print("Dequeue: %d" % queue.dequeue())
print("Dequeue: %d" % queue.dequeue())
print("Size: %d" % queue.size_queue())
