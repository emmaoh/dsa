# Dynamic and static data structures
Arrays are static data structure - we have to know the size of the data structures in advance(or we have to resize it)
Linked lists are dynamic data structures - They can grow organically based on the references(no resize operation needed)
# Random access (Random indexing)
Items in an array are located right next to each other in the main memory (RAM) this is why we use indexes
There is no random access in a linked list data structure
# Manipulating the first items
We have to shift several items (all the items in worst-case) when manipulating the first items in arrays - O(N)
Linked lists are dynamic data structures - we just have to update the references around the head node
# Manipulating the last items 
There can not be holes in the data structures when manipulating the last items in arrays 
Linked list have access to the first node(head node) exclusively so in this case we have to traverse the whole list in O(N) running time
# Memory management
Arrays do not need any extra memory
Linked lists needs extra memory because of the references(pointer)
# Searching for an arbitrary item (or removing an arbitrary item) takes O(N) linear running time for both data structures. 
                        Linked Lists |  Arrays 
Search              |       O(N)     |   O(1)
Insert at the start |       O(1)     |   O(N)
Insert at the end   |       O(N)     |   O(1)
Waste space         |       O(N)     |    0