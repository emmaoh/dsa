from dataclasses import dataclass


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None 


class LinkedList:
    def __init__(self):
        self.head = None
        self.num_of_nodes = 0 

    def insert(self, data):
        self.num_of_nodes += 1
        new_node = Node(data)
        if self.head is None:
            self.head = new_node 
        else:
            new_node.next = self.head 
            self.head = new_node 
        
    def size_of_list(self):
        return self.num_of_nodes

    #O(N) time complexity 
    def get_middle_node(self):

        fast_pointer = self.head
        slow_pointer = self.head 

        while fast_pointer.next and fast_pointer.next.next:
            fast_pointer = fast_pointer.next.next
            slow_pointer = slow_pointer.next
        
        return slow_pointer 

if __name__ == '__main__':
    linked_list = LinkedList()
    linked_list.insert(10)
    linked_list.insert(20)
    linked_list.insert(30)
    linked_list.insert(40)
    linked_list.insert(60)

    print(linked_list.get_middle_node().data)
