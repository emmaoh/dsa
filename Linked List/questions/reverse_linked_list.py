class Node:

    def __init__(self, data):
        self.data = data
        self.next = None

    def __repr__(self):
        return str(self.data)  

class LinkedList(object):

    def __init__(self):
        self.head = None
        self.num_of_nodes = 0 
    
    def size_of_list(self):
        return self.num_of_nodes 
    
    def insert(self, data):
        self.num_of_nodes += 1
        new_node = Node(data)

        if self.head is None:
            self.head = new_node
        else:
            new_node.next = self.head
            self.head = new_node

    def traverse_list(self):
        actual_node = self.head
        while actual_node is not None:
            print(actual_node)
            actual_node = actual_node.next

    #O(N) time complexity
    def reverse(self):
        cur = self.head 
        prev = None 
        next = None 

        while cur is not None:
            next = cur.next 
            cur.next = prev 
            prev = cur
            cur = next 
        
        self.head = prev 


if __name__ == "__main__":
    linked_list = LinkedList()
    linked_list.insert(12)
    linked_list.insert(122)
    linked_list.insert(3)
    linked_list.insert(31)
    linked_list.insert(10)
    linked_list.insert(11)
    linked_list.traverse_list()
    linked_list.reverse()
    print("Reversed list")
    linked_list.traverse_list()
 